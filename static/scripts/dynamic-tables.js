
function createTable(table, tableHead, tableFields)
{
    // const table = $(`<table class="data_view" id="${tableId}"></table>`);
    addRow(table, tableFields, tableHead, null, true);
    return table;
}

function clearTable(table)
{
    table.find('tr:has(td)').remove();
}

function addRow(table, tableFields, data, specialHandlers, isHeaderRow=false)
{
    const dataId = isHeaderRow ? 'h' : data.id;
    const tr = $(`<tr id="${dataId}"></tr>`).appendTo(table);
    tableFields.forEach(f =>
    {
        const id = `${dataId}:${f}`;
        const td = $(`<${isHeaderRow ? 'th' : 'td'} id="${id}"></isHeaderRow ? 'th' : 'td'>`).appendTo(tr);
        
        if (isHeaderRow)
            td.text(data[tableFields.indexOf(f)]);
        else if (f === '#')
            td.text(table.children('tr:has(td)').length);
        else if (f.startsWith('$'))
            specialHandlers[f.substring(1)](td, data);
        else
            td.text(data[f]);
    });
}

let socket;
const eventHandlers = { };

function generateId(length, possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789")
{
    let text = "";
    for (let i = 0; i < length; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    
    return text;
}

class WsEvent
{
    constructor(event, request_id, data = null)
    {
        if (typeof event === "object")
            Object.assign(this, event);
        else
        {
            this.event = event;
            this.request_id = request_id || generateId(8);
            this.data = data;
        }
    }
    
    encode()
    {
        const d = Object.assign({}, this);
        d.data = JSON.stringify(d.data);
        return JSON.stringify(d);
    }
    
    static decode(message)
    {
        const d = typeof message === "object" ? message : JSON.parse(message);
        d.data = JSON.parse(d.data);
        return new WsEvent(d);
    }
}

function wsConnect()
{ showAlert("info", "Connection established", false) }

function wsClose(event)
{
    const reason = `Code: ${event.code}; Reason: ${event.reason}`;
    if (event.wasClean)
        showAlert("info", `Connection closed (${reason})`);
    else
        showAlert("error", `Connection aborted (${reason})`);
    
    setTimeout(establishConnection, 500);
}

function wsEvent(event)
{
    console.log(`Data received: ${event.data}`);
    try
    {
        const decodedEvent = WsEvent.decode(event.data);
        console.log(`Event decoded: ${decodedEvent.event} #${decodedEvent.request_id}: '${decodedEvent.data}'`);
        eventHandlers[decodedEvent.event](decodedEvent);
    }
    catch (e)
    {
        showAlert("error", e);
        socket.close();
    }
}

function sendEvent(event)
{
    console.log(`Sending event: ${event.event} #${event.request_id}: '${event.data}'`);
    const message = event.encode();
    console.log(`Data sent: ${message}`);
    socket.send(message);
}

function wsError(error)
{ showAlert("error", `${error.message}`); }


function establishConnection()
{
    showAlert("warning", "Establishing connection...", false);
    socket = new WebSocket(`ws://${window.location.host}/ws`);
    socket.onopen = wsConnect;
    socket.onclose = wsClose;
    socket.onmessage = wsEvent;
    socket.onerror = wsError;
    return socket;
}

function addHandler(event, handler)
{
    eventHandlers[event] = handler;
}

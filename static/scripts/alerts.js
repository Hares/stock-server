const alertBoxId = 'alert_box';

function loadAlert()
{
    const link = document.querySelector('link[rel="import"]');
    const content = link.import;
    
    // Grab DOM from warning.html's document.
    const el = content.getElementById(alertBoxId);
    document.body.appendChild(el.cloneNode(true));
}

function showAlert(type="error", message="", show_prefix=true)
{
    const alertBox = document.getElementById(alertBoxId);
    let _html = '';
    _html += `<span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>`;
    if (show_prefix)
        _html += `<strong>${type.toUpperCase()}!</strong> `;
    _html += message;
    
    if (type === "error")
        console.error(message);
    else if (type === "warning")
        console.warn(message);
    else
        console.log(message);
    
    alertBox.className = `alert ${type}`;
    alertBox.innerHTML = _html;
    alertBox.style.display = "block";
}

function extractReason(data)
{
    let _reason = '';
    try
    { _reason = JSON.parse(data.reason); }
    catch (e) { }
    return _reason.message
        || _reason.reason
        || data.message
        || data.reason;
}

errorFunction = function (response)
{
    const data = response.responseJSON;
    if (!data)
    {
        const message = `Cannot perform request. Reason: Server not answered`;
        console.error(message);
        showAlert("error", message, true);
    }
    else if (response.status === 401)
    {
        const message = "Cannot perform request. Reason: Unauthorized";
        console.warn(message);
        showAlert("warning", message, false);
        document.location.replace(`/auth?from=${document.location}`);
    }
    else if (response.status === 404)
    {
        const message = "Cannot perform request. Reason: Position not found";
        console.warn(message);
        showAlert("warning", message, false);
    }
    else if (response.status === 410)
    {
        const message = "Cannot perform request. Reason: Not enough positions in stock";
        console.warn(message);
        showAlert("warning", message, false);
    }
    else
    {
        const message = `Cannot perform request. Reason: ${extractReason(data)}`;
        console.error(message);
        showAlert("error", message, true);
        console.error(data);
    }
};


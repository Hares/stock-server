const positions = [ ];

function loadPositions(cb)
{
    $.ajax({
        type: 'GET',
        url: `${stockApiPath}/positions`,
        error: errorFunction,
        success: function (data)
        {
            positions.length = 0;
            positions.push(...data.result);
            positions.sort(x => x.name);
            
            if (cb) cb();
        }
    });
}

function requestStockDelivery(data, cb)
{
    console.log(data);
    $.ajax({
        type: 'POST',
        async: true,
        url: `${stockApiPath}/delivery`,
        data: JSON.stringify({ positions: data }),
        contentType: "application/json",
        error: errorFunction,
        success: function (data)
        {
            showAlert("info", "Successfully requested");
            loadPositions(cb);
        }
    });
}

function checkAuth()
{
    $.ajax({
        type: 'POST',
        url: `${stockApiPath}/check-auth`,
        contentType: "application/json",
        error: errorFunction,
    });
}

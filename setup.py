from setuptools import setup

setup \
(
    name = 'stock-server',
    version = '0.1',
    requires = 
    [
        'dataclasses-json',
        'http-server-base>=1.6.0.2'
    ],
    packages =
    [
        'stock_server',
        'stock_server.handlers',
    ],
    package_dir = {'': 'src'},
    url = 'https://gitlab.com/Hares/stock-server',
    license = 'MIT',
    author = 'Peter Zaitcev',
    author_email = 'zaitcev.po@gmail.com',
    description = 'Example stock server',
)

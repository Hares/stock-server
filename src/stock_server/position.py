from typing import Optional

from dataclasses import dataclass
from dataclasses_json import DataClassJsonMixin

@dataclass
class Position(DataClassJsonMixin):
    id: str
    count: int
    name: str
    description: str

@dataclass
class PositionUpdate(DataClassJsonMixin):
    id: str
    count: int

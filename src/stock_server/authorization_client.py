from hashlib import sha256
import json
from dataclasses import dataclass, field
from typing import Dict, List, Optional

from http_server_base.tools import ConfigLoader

@dataclass
class Credential:
    user_hash: str
    password_hash: str
    permissions: List[str]

@dataclass
class AuthorizationClient:
    credentials_storage_path: str = None
    credentials_base: Dict[str, Credential] = field(init=False)
    
    def __post_init__(self):
        if (self.credentials_storage_path is None):
            self.credentials_storage_path = ConfigLoader.get_from_config('userCredentialStorage')
        self.load_credentials()
    
    def load_credentials(self):
        self.credentials_base = dict()
        with open(self.credentials_storage_path) as f:
            for u, v in json.load(f).items():
                pwd, _, perm = v.partition('|') # type: str, str, str
                perm: List[str] = perm.split(',')
                self.credentials_base[u] = Credential(u, pwd, perm)
    
    def check_credentials(self, user: str, password: str) -> Optional[List[str]]:
        user_hash = sha256(user.encode()).hexdigest()
        password_hash = sha256(password.encode()).hexdigest()
        
        c = self.credentials_base.get(user_hash)
        if (c is not None and c.password_hash == password_hash):
            return c.permissions
        else:
            return None

from http_server_base.restapi import PathTreeRest_RequestHandler, rest_method

class HTML_RequestHandler(PathTreeRest_RequestHandler):
    logger_name = 'stock-server.html'
    base_path = '/'
    
    @rest_method('/')
    @rest_method('/{path}')
    def get_page(self, path: str = '/'):
        if (path in ('/', '')):
            path = 'index'
        if (not path.endswith('.html')):
            path += '.html'
        
        try:
            self.render(path)
        except FileNotFoundError:
            self.logger.warning(f"File '{path}' not found")
            self.redirect('/')

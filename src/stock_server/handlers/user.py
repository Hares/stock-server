from typing import List

from dataclasses import dataclass
from dataclasses_json import DataClassJsonMixin

@dataclass
class User(DataClassJsonMixin):
    username: str
    permissions: List[str]

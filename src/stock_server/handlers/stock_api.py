from typing import List, Optional
from dataclasses import dataclass
from dataclasses_json import DataClassJsonMixin

from http_server_base.restapi import PathTreeRest_RequestHandler, rest_method
from http_server_base.tools import ServerError, ConfigLoader
from marshmallow import ValidationError
from tornado.locks import Lock
from tornado.web import authenticated

from stock_server import PositionUpdate, StockManager, AuthorizationClient, Position

from . import User, BaseHandler

lock = Lock()
class StockApi_RequestHandler(PathTreeRest_RequestHandler, BaseHandler):
    logger_name = 'stock-server.api'
    base_path = '/stock-api'
    
    stock_manager: StockManager
    auth_manager: AuthorizationClient
    
    # noinspection PyMethodOverriding
    def initialize(self, *, stock_manager: StockManager, auth_manager: AuthorizationClient, **kwargs):
        super().initialize(**kwargs)
        self.stock_manager = stock_manager
        self.auth_manager = auth_manager
    
    @rest_method('/authorize', method='GET')
    async def auth(self):
        # noinspection PyAsyncCall
        self.render('auth.html')
    
    @rest_method('/authorize', method='POST', body_args=[ 'username', 'password' ], query_args=[ '?from' ])
    async def auth(self, username: str, password: str, **kwargs):
        permissions = self.auth_manager.check_credentials(username, password)
        if (permissions is None):
            raise ServerError(400, message="Wrong credentials")
        
        user = User(username, permissions)
        self.set_secure_cookie('user_info', User.schema().dumps(user))
        
        _from = kwargs.get('from')
        if (not _from):
            _from = '/'
        self.redirect(_from)
    
    @rest_method('/check-auth', method='*')
    def check_auth(self, permission: str = None):
        super().check_auth(permission)
    
    @rest_method('/positions', simple_return=True)
    async def get_positions(self):
        self.check_auth('stock')
        async with lock:
            return self.stock_manager.serialize()
    
    @rest_method('/positions/{positionId}', simple_return=True, support_arguments_capitalization_style_switch=True)
    async def get_positions(self, position_id):
        self.check_auth('stock')
        async with lock:
            try:
                return self.stock_manager.positions[position_id]
            except KeyError:
                raise ServerError(404, message=f"No such position: '{position_id}'")
    
    @rest_method('/delivery', method='POST', body_args=[ ('positions', list) ])
    async def request_delivery(self, positions: List):
        self.check_auth('stock')
        try:
            positions: List[PositionUpdate] = PositionUpdate.schema().load(positions, many=True)
        except ValidationError:
            raise ServerError(400, message="Invalid body")
        async with lock:
            for u in positions:
                stock_pos = self.stock_manager.positions.get(u.id, None)
                if (stock_pos is None):
                    raise ServerError(404, message=f"No such position: '{u.id}'")
                if (not isinstance(u.count, int) or u.count <= 0):
                    raise ServerError(400, message=f"Position '{u.id}' ('{stock_pos.name}'): count should be positive integer.")
                if (u.count > stock_pos.count):
                    raise ServerError(410, message=f"Position '{u.id}' ('{stock_pos.name}'): not enough positions in stock.")
            
            if (not self.stock_manager.request_position_delivery(positions)):
                raise ServerError(503, message="Stock is not online")
        
        self.resp_success(201)

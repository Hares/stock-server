from typing import Optional

from http_server_base import Logged_RequestHandler
from http_server_base.tools import ServerError

from . import User

class BaseHandler(Logged_RequestHandler):
    def get_current_user(self) -> Optional[User]:
        c = self.get_secure_cookie('user_info')
        if (c is not None):
            return User.schema().loads(c)
        else:
            return None
    
    @property
    def current_user(self) -> Optional[User]:
        return super().current_user
    
    def check_auth(self, permission: str = None):
        if (self.current_user is None):
            raise ServerError(401)
        if (permission is not None and not permission in self.current_user.permissions):
            raise ServerError(403)

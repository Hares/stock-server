import json
from json import JSONDecodeError
from typing import List, Optional, Any
from dataclasses import dataclass
from dataclasses_json import DataClassJsonMixin
from http_server_base import Logged_RequestHandler

from http_server_base.restapi import PathTreeRest_RequestHandler, rest_method
from http_server_base.tools import ServerError, ConfigLoader
from marshmallow import ValidationError
from tornado.locks import Lock
from tornado.web import authenticated
from tornado.websocket import WebSocketHandler

from stock_server import IStockAdminConnection, PositionUpdate, StockManager
from . import BaseHandler

@dataclass()
class Event(DataClassJsonMixin):
    request_id: str
    event: str
    data: Optional[str] = None

lock = Lock()
class StockApi_WebsocketRequestHandler(WebSocketHandler, BaseHandler, IStockAdminConnection):
    logger_name = 'stock-server.ws-api'
    base_path = '/ws'
    
    stock_manager: StockManager
    
    # noinspection PyMethodOverriding
    def initialize(self, *, stock_manager: StockManager, **kwargs):
        super().initialize(**kwargs)
        self.stock_manager = stock_manager
    
    def open(self, *args, **kwargs):
        try:
            self.check_auth('admin')
        except ServerError:
            self.close(1006, "Unauthorized")
            return
        
        self.send_event(Event(self.request_id, 'init_resp', f"Hi, {self.get_user_info()}"))
        self.stock_manager.stock_admin_connections.append(self)
    
    def on_message(self, message):
        e = self.read_event(message)
        if (e is None):
            return
        
        events_map = dict \
        (
            delivery_resp =   self.handle_delivery_resp,
            requests_req =    self.handle_requests_req,
            connections_req = self.handle_connections_req,
        )
        if (e.event not in events_map):
            self.custom_error(1003, f"Invalid event: {e.event}")
        
        events_map[e.event](e)
    
    def handle_delivery_resp(self, e: Event):
        if (not isinstance(e.data, bool)):
            self.custom_error(1003, f"Invalid data type: {type(e.data)}")
        if (e.request_id not in self.stock_manager.active_delivery_requests):
            self.custom_error(1003, f"Invalid request id: {e.request_id}")
        self.stock_manager.accept_delivery_request(e.request_id, e.data)
    
    def handle_requests_req(self, e: Event = None):
        req_id = self.request_id if (e is None) else e.request_id
        requests = { r_id: PositionUpdate.schema().dump(r, many=True) for r_id, r in self.stock_manager.active_delivery_requests.items() }
        self.send_event(Event(req_id, 'requests_resp', requests))
    
    def handle_connections_req(self, e: Event = None):
        req_id = self.request_id if (e is None) else e.request_id
        connections = [ h.get_user_info() for h in self.stock_manager.stock_admin_connections ]
        self.send_event(Event(req_id, 'connections_resp', connections))
    
    def on_close(self):
        self.logger.info("WebSocket closed")
        self.stock_manager.stock_admin_connections.remove(self)
    
    def get_user_info(self):
        return f"{self.current_user.username} (ip={self.request.remote_ip})"
    def clear_request(self, request_id: str):
        self.send_event(Event(request_id, 'remove_request_resp'))
    def handle_request(self, request_id: str, positions: List[PositionUpdate]) -> bool:
        positions_encoded = PositionUpdate.schema().dump(positions, many=True)
        self.send_event(Event(event='delivery_req', request_id=request_id, data=positions_encoded))
        return True
    
    
    def send_event(self, e: Event):
        self.logger.debug(f"Sending event: {e}")
        if (e.data is not None):
            e.data = json.dumps(e.data)
        self.write_message(Event.schema().dump(e))
    def read_event(self, message: str) -> Optional[Event]:
        try:
            e = Event.schema().loads(message)
            if (e.data is not None):
                e.data = json.loads(e.data)
        except Exception as e:
            self.custom_error(1003, "Invalid event body")
        else:
            self.logger.debug(f"Reading event: {e}")
            return e
    def custom_error(self, code=None, reason=None, http_error=400):
        msg = f"Error code {code}: {reason}"
        self.logger.warning(msg)
        self.send_event(Event(self.request_id, 'bye', data=msg))
        self.close(http_error, reason)
        raise ServerError(code, reason)

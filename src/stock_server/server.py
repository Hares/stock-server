from dataclasses import dataclass, field
from typing import Type

from http_server_base import JsonBasicResponder, IResponder
from http_server_base.ab2 import ApplicationBase2
from http_server_base.restapi import PrefixTreeRestRouter
from tornado.routing import RuleRouter
from tornado.web import StaticFileHandler

from stock_server import StockManager, AuthorizationClient
from stock_server.handlers import StockApi_RequestHandler, HTML_RequestHandler, StockApi_WebsocketRequestHandler

@dataclass
class StockServer(ApplicationBase2):
    name: str = "Stock Server"
    logger_name: str = 'stock-server'
    
    internal_router_class: Type[RuleRouter] = PrefixTreeRestRouter
    responder_class: Type[IResponder] = JsonBasicResponder
    
    stock_manager: StockManager = field(init=False, default_factory=StockManager)
    auth_manager: AuthorizationClient = field(init=False, default_factory=AuthorizationClient)
    
    def __post_init__(self, *args, **kwargs):
        self.handlers = \
        [
            ('/static/', StaticFileHandler, dict(path='static/')),
            ('/ws', StockApi_WebsocketRequestHandler, dict(stock_manager=self.stock_manager)),
            (HTML_RequestHandler, ),
            (StockApi_RequestHandler, dict(stock_manager=self.stock_manager, auth_manager=self.auth_manager)),
        ]
        super().__post_init__(*args, **kwargs)

if (__name__ == '__main__'):
    StockServer.simple_start_server()

from typing import List, Dict, Any
from dataclasses import dataclass, field
import json
from uuid import uuid4

from http_server_base.tools import ConfigLoader

from . import Position, PositionUpdate, IStockAdminConnection

@dataclass
class StockManager:
    stock_path: str = None
    encoding: str = 'utf-8'
    positions: Dict[str, Position] = field(init=False, default=None)
    stock_admin_connections: List[IStockAdminConnection] = field(init=False, default_factory=list)
    active_delivery_requests: Dict[str, List[PositionUpdate]] = field(init=False, default_factory=dict)
    
    def __post_init__(self):
        if (self.stock_path is None):
            self.stock_path = ConfigLoader.get_from_config('stockPath')
        self.load_positions()
    
    def load_positions(self):
        with open(self.stock_path, 'rb') as sb:
            j = json.loads(sb.read().decode(self.encoding))
            values: List[Position] = Position.schema().load(j, many=True)
            self.positions = { v.id: v for v in values }
    
    def save_positions(self):
        with open(self.stock_path, 'wb') as sb:
            j = self.serialize()
            s = json.dumps(j, indent=4, ensure_ascii=False)
            sb.write(s.encode(self.encoding))
    
    def update_position(self, id: str, increment: int, *, save: bool = True):
        self.positions[id].count += increment
        if (save):
            self.save_positions()
    
    def serialize(self) -> List[Dict[str, Any]]:
        v: List[Position] = list(self.positions.values())
        v.sort(key=lambda x: x.id)
        j: List[Dict[str, Any]] = Position.schema().dump(v, many=True)
        return j
    
    def request_position_delivery(self, positions: List[PositionUpdate]) -> bool:
        
        request_id = str(uuid4())
        self.active_delivery_requests[request_id] = positions
        for handler in self.stock_admin_connections:
            handler.handle_request(request_id, positions=positions)
        
        if (not self.stock_admin_connections):
            return False
        else:
            return True
    
    def accept_delivery_request(self, request_id: str, accepted: bool = True):
        for handler in self.stock_admin_connections:
            handler.clear_request(request_id)
        
        updates = self.active_delivery_requests[request_id]
        if (accepted):
            for u in updates:
                self.update_position(u.id, -u.count)
        del self.active_delivery_requests[request_id]

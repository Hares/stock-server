from typing import List

from stock_server import PositionUpdate

class IStockAdminConnection():
    def handle_request(self, request_id: str, positions: List[PositionUpdate]) -> bool:
        pass
    def clear_request(self, request_id: str):
        pass
    def get_user_info(self) -> str:
        pass

# Example Stock Server

This is a stock client-server application implementation.
It provides HTTP REST Api for clients and WebSocket Api stock managers.

### Principe of work
1. Server is [Python 3.7](https://www.python.org/downloads/release/python-372/) with Tornado 6 and HTTP Server Base frameworks.
2. Both Client and Stock are HTML template rendered by server, and a number of JS6 scripts with jQuery and Ajax.
3. Client connects to the Server via HTTP REST Api.
4. Stock connects to the Server via WebSocket Api. 
5. Both Client and Stock use Cookie authorization.
6. Server supports multiple stock operators at the same time.

##### Client - Server Channel
```mermaid
sequenceDiagram
    participant Client
    participant Server
    participant Stock
    
    Client ->> Server:  Get positions
    Server -->> Client: 200 OK
    Client ->> Server:  Delivery request
    alt Stock is online
        Server ->> Stock: delivery_req
        Server -->> Client: 201 Created
    else
        Server -->> Client: 503 Temprorary Unavailable
    end
    Note right of Server: Server stores delivery request regardless of stock state.
```

##### Server - Stock Channel
```mermaid
sequenceDiagram
    participant Server
    participant Stock
    
    Stock ->> Server:  establish connection
    Server -->> Stock: init_resp
    Stock ->> Server:  requests_req
    Server -->> Stock: requests_resp
    
    Note over Server, Stock: Client creates new delivery request
    Server ->> Stock:  delivery_req
    Note right of Stock: Stock operator either accepts or denies the request
    Stock -->> Server: delivery_resp
    Note over Server, Stock: Server sends special event to all stock clients, however, the initator will ignore it.
    Server ->> Stock:  remove_request_resp
    Stock ->> Server:  requests_req
    Server -->> Stock: requests_resp
```

### HTTP Api Reference
Base path: `/stock-api`

Server always responds in JSON format, see [Model -> Response](#response).
Any data is encoded inside `result` field.

##### Authorization
 - `POST /authorize`
 - Consumes: form-data parameters
 - Arguments:
   - username: str
   - password: str
 - Returns: 200 OK
 - Returns 400 if credentials are wrong.
 - Sets cookies in response.

##### Check Authorization
 - `ANY /check-auth`
 - Returns: 200 OK
 - Returns OK if authorization is successful, 401 otherwise.

##### Request Positions
 - `GET /positions`
 - Returns: 200 OK
 - Returns list of positions in stock, see [Model -> PositionList](#position)

##### Request Position
 - `GET /positions/{positionId}`
 - Returns: 200 OK
 - Returns single position information, see [Model -> Position](#position)

##### Request Delivery
 - `POST /delivery`
 - Consumes: application/json, see [Model -> PositionList](#positionlist)
 - Returns: 201 Created
 - Returns 400 if user sent wrong request (i.e., malformed model)
 - Returns 403 if user has no permissions to request from stock
 - Returns 404 if user sent wrong position id
 - Returns 410 if there are not enough positions in stock
 - Returns 503 if no stock operators are online


### WebSocket Api Reference
WebSocket URL: `/ws`

All messages are JSON-encoded events which contain event name, request id and some custom data.
Data is always encoded JSON (so, messages could contain double-encoded JSON).

Client always opens connection first, then server responds `init_resp` event.
Then both client and server could send events (see event table below).
If client sends malformed message, server sends `bye` event, and then closes connection.
Client usually then tries to reconnect.

|         Event         |                       Data type                      | Description                                                                                    |
|:---------------------:|:----------------------------------------------------:|------------------------------------------------------------------------------------------------|
| `init_resp`           |                          str                         | Initial response from server. Data contains hello message.                                     |
| `bye`                 |                          str                         | Server response to malformed message, it then closes connection.                               |
| `delivery_req`        |       List of [PositionUpdate](#positionupdate)      | New delivery request. Server sends this event to all his clients.                              |
| `delivery_resp`       |                         bool                         | Close the delivery request with appropriate status (true: accepted, false: denied)             |
| `remove_request_resp` |                           -                          | The delivery request is closed. Server sends this event to all his clients.                    |
| `connections_req`     |                           -                          | Client could send this event to request information about all clients connected to the server. |
| `connections_resp`    |                      List of str                     | List of all active connections. Strings contain username and IP.                               |
| `requests_req`        |                           -                          | Client could send this event to request information about all active delivery requests.        |
| `requests_resp`       | Map: id -> List of [PositionUpdate](#positionupdate) | All active delivery requests.                                                                  |

### Model Reference
##### Response
 - code: int
 - success: bool
 - reason: str
 - result: Any (optional)
 - message: str (optional)

##### PositionList
 - positions: List of Position

##### Position
 - id: str
 - name: str
 - description: str
 - count: int

##### PositionUpdate
 - id: str
 - count: int 

##### Event
 - request_id: str
 - event: str
 - data: Encoded JSON

### Links
 - [Python 3.7 Release Notes](https://docs.python.org/3.7/whatsnew/3.7.html)
 - [Tornado 6](https://www.tornadoweb.org/en/stable/)
 - [HTTP Server Base](https://gitlab.com/Hares/http-server-base)
 - [ECMAScript 6 Features](http://es6-features.org/#Constants)
 - [jQuery and Ajax](https://jquery.com/)
